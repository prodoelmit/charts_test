#include "mw.h"
#include "ui_mw.h"
#include <QSvgGenerator>

MW::MW(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MW),
	m_xAxis(0),
	m_yAxis1(0),
	m_yAxis2(0)
{
	ui->setupUi(this);
	parseCSV();
	updateSettings();
	ui->chartWidget->setRenderHint(QPainter::Antialiasing, true);

	QShortcut* ctrlc = new QShortcut(QKeySequence("Ctrl+C,S"),this);
	connect(ctrlc,SIGNAL(activated()),this,SLOT(copySVGToClipboard()));

}

MW::~MW()
{
	delete ui;
}

void MW::parseCSV()
{
	QString input = ui->csvEdit->toPlainText();
	QStringList lines = input.split("\n",QString::SkipEmptyParts);
	m_headers = lines.takeFirst().split(",");
	m_data.clear();
	foreach (QString row, lines) {
		QList<qreal> values;
		QList<QString> valStrings = row.split(",");
		foreach (QString valString, valStrings) {
			values << valString.toDouble();
		}
		m_data.append(values);
	}
	int xCol = m_XCol;
	QList<int> yCols = m_YCols;

	ui->xValuesCB->clear();
	ui->xValuesCB->addItems(m_headers);
	ui->xValuesCB->setCurrentIndex(xCol);

	ui->yValuesLW->clear();
	ui->yValuesLW->addItems(m_headers);
	foreach (int row, yCols) {
		if (ui->yValuesLW->item(row)) {
			ui->yValuesLW->item(row)->setSelected(true);
		}
	}
}

void MW::updateSettings()
{

	m_YCols.clear();
	m_XCol = 0;

	m_XCol = ui->xValuesCB->currentIndex();
	QItemSelectionModel* selModel = ui->yValuesLW->selectionModel();
	foreach (QModelIndex i, selModel->selectedIndexes()) {
		m_YCols.append(i.row());
	}
}

void MW::drawPlot()
{

	QChart* chart = ui->chartWidget->chart();
	chart->removeAxis(chart->axisX());
	chart->removeAxis(chart->axisY());


	QString seriesType = ui->styleCB->currentText();




	qDeleteAll(m_series);
	m_series.clear();

	QList<QList<qreal> > selectedData;

	foreach (QList<qreal> row, m_data) {
		QList<qreal> selectedDataRow;
		selectedDataRow << row[m_XCol];
		foreach (int index, m_YCols) {
			selectedDataRow << row[index];
		}
		selectedData << selectedDataRow;
	}

	int yCount = m_YCols.size();

	m_xAxis = new QValueAxis(chart);
	m_yAxis1 = new QValueAxis(chart);
	chart->setAxisX(m_xAxis);
	chart->setAxisY(m_yAxis1);


	if (seriesType == "Line") {
		for (int i = 0; i < yCount; i++) {
			QLineSeries* s = new QLineSeries(chart);
			s->setName(m_headers.at(i));
			chart->addSeries(s);
			s->attachAxis(m_yAxis1);
			s->attachAxis(m_xAxis);
			m_series << s;
		}
	} else {
		for (int i = 0; i < yCount; i++) {
			QScatterSeries* s = new QScatterSeries(chart);
			s->setName(m_headers.at(i));
			chart->addSeries(s);
			s->attachAxis(m_yAxis1);
			s->attachAxis(m_xAxis);
			m_series << s;
		}
	}

	foreach (QList<qreal> row, selectedData) {
		qreal x = row.takeFirst();
		for (int i = 0; i < yCount; i++) {
			qreal y = row[i];
			QPointF p(x,y);
			m_series[i]->append(x,y);
			qDebug() << i << x << y;
		}

	}

	m_xAxis->setRange(-5, 10);
	m_yAxis1->setRange(0,20);

	chart->setTitle("WHat's that?" );


}

void MW::copySVGToClipboard()
{
	QBuffer b;
	QSvgGenerator p;
	p.setOutputDevice(&b);
	p.setSize(QSize(600,320));
	p.setViewBox(QRect(0,0,600,320));
	QPainter painter;
	painter.begin(&p);
	painter.setRenderHint(QPainter::Antialiasing);
	QSize size = ui->chartWidget->size();
//	ui->chartWidget->resize(600,320);
	ui->chartWidget->render(&painter);
	qDebug() << "Copied";
	ui->chartWidget->resize(size);
	painter.end();
	QMimeData * d = new QMimeData();
	d->setData("image/svg+xml",b.buffer());
	QApplication::clipboard()->setMimeData(d,QClipboard::Clipboard);

}
