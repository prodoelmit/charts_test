#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <QList>
#include <QString>
#include <QtCharts>
using namespace QtCharts;

namespace Ui {
class MW;
}

class MW : public QMainWindow
{
	Q_OBJECT

public:
	explicit MW(QWidget *parent = 0);
	~MW();

public slots:
	void parseCSV();
	void updateSettings();
	void drawPlot();
	void copySVGToClipboard();

private:
	Ui::MW *ui;
	QList<QString> m_headers;
	QList<QList<qreal> > m_data;
	QValueAxis* m_xAxis;
	QValueAxis* m_yAxis1;
	QValueAxis* m_yAxis2;
	QList<QXYSeries*> m_series;

	QList<int> m_YCols;
	int m_XCol;

};

#endif // MW_H
