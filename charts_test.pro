#-------------------------------------------------
#
# Project created by QtCreator 2016-08-02T00:34:58
#
#-------------------------------------------------

QT       += core gui charts svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = charts_test
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp

HEADERS  += mw.h

FORMS    += mw.ui
